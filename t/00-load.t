#!perl -T

use Test::More tests => 1;

BEGIN {
    use_ok( 'Business::BatchPayment::KeyBank' ) || print "Bail out!\n";
}

diag( "Testing Business::BatchPayment::KeyBank $Business::BatchPayment::KeyBank::VERSION, Perl $], $^X" );
